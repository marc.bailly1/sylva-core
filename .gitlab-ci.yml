stages:
  - generate
  - test
  - deploy

default:
  tags:
    - gitlab-org-docker
    # - docker
  image: registry.gitlab.com/sylva-projects/sylva-elements/container-images/ci-image:v1-0-1
  services:
    - name: docker:dind
      alias: docker

variables:
  DOCKER_TLS_CERTDIR: ""
  GITLEAKS_ARGS: '--verbose --log-opts=$CI_COMMIT_SHA'

# included templates
include:
  # Gitleaks template
  - project: "to-be-continuous/gitleaks"
    ref: 2.1.1
    file: "templates/gitlab-ci-gitleaks.yml"

chart-generator:
  stage: generate
  script:
    - echo "Generate a job for each modified charts"
    - ./tools/gci-templates/scripts/generate-pipeline.sh > generated-pipeline.yml
  artifacts:
    expire_in: 1 hour
    paths:
      - generated-pipeline.yml
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - charts/**/*
        - tools/gci-templates/**/*

chart-jobs:
  stage: generate
  needs:
    - chart-generator
  trigger:
    include:
      - artifact: generated-pipeline.yml
        job: chart-generator
    strategy: depend
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - charts/**/*
        - tools/gci-templates/**/*
        - tools/yaml2json.py

yamllint:
  stage: test
  script:
    - 'yamllint . -d "$(cat < tools/gci-templates/yamllint.yaml) $(cat < tools/gci-templates/yamllint-helm-exclude-charts.yaml)"'
  only:
    - merge_requests

avoid-typo-on-bootstrap:
  stage: test
  script:
    - |
      rm -rf .git  # because busybox grep does not support --exclude-dir
      echo "Check against frequent typos on 'bootstrap'..."
      set +e
      typos=$(grep -rnsiE 'boostrap|bootrap|bootsrap' . | grep -v '.gitlab-ci.yaml:      typos=')
      set -e
      if [ -n "$typos" ]; then
        echo "A few typos were found on the 'bootstrap' word:"
        echo "-----------------"
        echo "$typos"
        echo "-----------------"
        exit 1
      fi
  only:
    - merge_requests

check-docs-markdown:
  image: registry.gitlab.com/gitlab-org/gitlab-docs/lint-markdown:alpine-3.16-vale-2.20.2-markdownlint-0.32.2-markdownlint2-0.5.1
  stage: test
  script:
    - git fetch origin $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
    - |
      md_files=$(git diff --name-only $CI_COMMIT_SHA origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME | grep "\.md$" || true)
      if [ -n "$md_files" ] ; then
        markdownlint-cli2-config tools/gci-templates/.markdownlint.yml $md_files
      else
        echo "No modified .md files"
      fi
  only:
    refs:
      - merge_requests
    changes:
      - "**/*.md"

.test-capd:
  stage: deploy
  script:
    - echo -e "\e[1m\e[0Ksection_start:`date +%s`:gitlab_ci_prepare[collapsed=true]\r\e[0KRunning .gitlab-ci script step\e[0m"

    - DOCKER_IP=$(getent hosts docker | awk '{print $1}')
    - |
      # create a cluster with access to API endpoint through docker-in-docker address
      cat <<EOF | kind create cluster --name capd --config=-
      kind: Cluster
      apiVersion: kind.x-k8s.io/v1alpha4
      networking:
        apiServerAddress: "$DOCKER_IP"
        apiServerPort: 6443
      EOF
    - kubectl cluster-info --context kind-capd

    - KIND_PREFIX=$(docker network inspect kind -f '{{ (index .IPAM.Config 0).Subnet }}')
    - ip route add $KIND_PREFIX via $DOCKER_IP

    - export DOCKER_HOST=tcp://$DOCKER_IP:2375
    - values_file=environment-values/${ENV_NAME}/values.yaml
    - yq -i '.cluster.capd.docker_host = strenv(DOCKER_HOST)' $values_file
    - export CLUSTER_EXTERNAL_IP=$(echo $KIND_PREFIX | awk -F"." '{print $1"."$2"."$3".100"}')
    - yq -i '.cluster.cluster_external_ip = strenv(CLUSTER_EXTERNAL_IP)' $values_file
    - if [[ -n $DOCKERIO_REGISTRY_MIRROR ]]; then yq -i '.dockerio_registry_mirror = strenv(DOCKERIO_REGISTRY_MIRROR)' $values_file; fi
    # for now, some units don't work on generic workers
    # (see https://gitlab.com/sylva-projects/sylva-core/-/issues/2)
    - |
      if [[ ! $CI_RUNNER_TAGS =~ ".*o-romania.*" ]]; then
        echo "disable some units to allow capd jobs to run on generic gitlab runners"
        yq -i '.units.workload-cluster.enabled=false' $values_file
        yq -i '.units.workload-cluster-calico.enabled=false' $values_file
        yq -i '.units.monitoring.enabled=false' $values_file
        yq -i '.units.keycloak.enabled=false' $values_file
        yq -i '.units.flux-webui.enabled=false' $values_file
        yq -i '.units.capi-rancher-import.enabled=false' $values_file
      fi
    - export DEBUG_ON_EXIT=1
    - echo -e "\e[0Ksection_end:`date +%s`:gitlab_ci_prepare\r\e[0K"
    - ./bootstrap.sh environment-values/${ENV_NAME}
    - kubectl --kubeconfig management-cluster-kubeconfig get pod --selector job-name=check-rancher-clusters-job -o=jsonpath='{.items[?(@.status.phase=="Succeeded")].metadata.name}' | xargs --no-run-if-empty -n1 kubectl --kubeconfig management-cluster-kubeconfig logs || true
  artifacts:
    when: on_failure
    expire_in: 6 hour
    paths:
      - debug-on-exit.log
      - bootstrap-cluster-dump/
      - management-cluster-dump/

  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_COMMIT_TAG
    - if: $CI_MERGE_REQUEST_LABELS =~ /run-e2e-tests/
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      when: manual
  allow_failure: true

test-rke2-capd:
  timeout: 60min
  extends: .test-capd
  variables:
    ENV_NAME: rke2-capd
  rules:
    #NOTE: rke2-capd is resource intensive (~140GB disk), so test it only when needed by adding label `run-e2e-tests` to MR
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event' && $CI_MERGE_REQUEST_LABELS =~ /run-e2e-tests/
      when: manual
  tags:
    - o-romania-bm

test-kubeadm-capd:
  timeout: 45min
  extends: .test-capd
  variables:
    ENV_NAME: kubeadm-capd

test-kubeadm-capd-bm:
  timeout: 45min
  extends: .test-capd
  rules:
    #NOTE: testing kubeadm-capd stack on managed runner by adding label `run-e2e-tests` to MR
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event' && $CI_MERGE_REQUEST_LABELS =~ /run-e2e-tests/
      when: manual
  tags:
    - o-romania-bm
